
//**************************************************************
// Map Datum -- Boxstation
//**************************************************************

/datum/map/active
	nameShort = "debug"
	nameLong = "Debug Station"
	map_dir = "debugstation"
	tDomeX = 20
	tDomeY = 24
	tDomeZ = 2
	zLevels = list(
		/datum/zLevel/station,
		/datum/zLevel/centcomm,
		/datum/zLevel/mining,
		)

////////////////////////////////////////////////////////////////
//#include "defficiency/pipes.dm" // Atmos layered pipes.
//#include "debug.dmm"